# index.ts
Entry ponit to the application. 
Here the app is being initialised, input type instantiated,
created an instance of Game class.

# game.ts
File contains Game object where most of the game logic is implemented.

# utils
Some functions used by the app stored here. 
In case of the future requirements to restructure the app this code could be easily reused.

## Install application dependencies:
```
npm install
```

## Start application:
```
npm start
```

## Test application:
```
npm test
```