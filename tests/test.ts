import { Game } from '../src/game';
import { formatInput, getTextMessage } from '../src/utils';

describe('Test Utils', function () {
    it('formatInput', function () {
        let result = formatInput('3\n');
        expect(result).toBe(3);
    });

    it('getTextMessage', function () {
        let result = getTextMessage('next');
        expect(result).toBe('>> Please enter the next number');
    });
});

describe('Test Game', function () {
    it('getNumbersList', function () {
        const game = new Game(1000);
        let result = game.getNumbersList();
        expect(result).toEqual({});
    });
});