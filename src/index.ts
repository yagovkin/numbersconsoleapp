import { formatInput } from './utils';
import { Game } from './game';

const game = new Game(1000);

// Get process.stdin as the standard input object.
const standard_input = process.stdin;

// Set input character encoding.
standard_input.setEncoding('utf-8');

// When user input data and click enter key.
standard_input.on('data', function (data) {
	const input = formatInput(data);

	game.processInput(input);
});