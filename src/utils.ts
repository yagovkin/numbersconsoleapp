export const buildFibArray = (numMax: number): Array<number> => {
	const fibArray: Array<number> = [0, 1];
	for (let i = fibArray.length; i < numMax; i++) {
		fibArray[i] = fibArray[i - 2] + fibArray[i - 1];
	}

	return fibArray;
};

export const getTextMessage = (option: string) => {
	switch (option) {
		case 'greeting':
			return ('>> Please input the number of time in seconds between emitting numbersList and their frequency');
		case 'first':
			return ('>> Please enter the first number');
		case 'next':
			return ('>> Please enter the next number');
		case 'fib':
			return ('>> FIB');
		case 'warning':
			return ('>> Input value must be an Integer!');
		case 'halt':
			return ('timer halted');
		case 'resume':
			return ('timer resumed');
		case 'quit':
			return (">> Thanks for playing, press any key to exit.");
		default:
			return;
	}
};

export const formatInput = (value: string) => {
	const allowedStrings = ['halt', 'resume', 'quit'];
	let processedStr: string = value.replace(/(\r\n|\n|\r)/gm, '').trim();

	if (processedStr.length > 0) {
		const processedInt: number = Number(processedStr);

		if (Number.isInteger(processedInt)) {
			return processedInt;
		} else {
			processedStr = processedStr.toLowerCase();
			return allowedStrings.includes(processedStr) ? processedStr : null;
		}
	} else {
		return null;
	}
};

export const formatNumbersList = (numbersList: object) => {
	if (Object.keys(numbersList).length > 0) {
		let sortedNumbers = [];
		let output = '';

		for (let num in numbersList) {
			sortedNumbers.push([num, numbersList[num]]);
		}

		sortedNumbers.sort(function (a, b) {
			return b[1] - a[1];
		});

		sortedNumbers.forEach(function (num, i) {
			output = `${output}${num[0]}: ${num[1]}${i === sortedNumbers.length - 1 ? '' : ', '}`;
		});

		return `>> ${output}`;
	}
};