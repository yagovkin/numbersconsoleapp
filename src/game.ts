import { buildFibArray, getTextMessage, formatNumbersList } from './utils';

export class Game {
    timer: NodeJS.Timeout;
    interval: number;
    numbersList: object;
    fibArray: Array<number>;

    isFib = (num) => this.fibArray.includes(num);
    startTimer = () => setInterval(() => {
        if (Object.entries(this.numbersList).length) {
            this.printNumbersList();
        };
    }, this.interval * 1000);
    addToNumbersList = (num) => this.numbersList[num] = this.numbersList[num] ? this.numbersList[num] + 1 : 1;
    setInterval = (num) => { this.interval = Number(num) };

    printLine = (message) => console.log(getTextMessage(message));
    printNumbersList = () => console.log(formatNumbersList(this.numbersList));

    public getNumbersList = () => this.numbersList;

    constructor(fibMax: number) {
        this.numbersList = {};
        this.fibArray = buildFibArray(fibMax);

        // Prompt user to input data in console.
        this.printLine('greeting');
    };

    // Main game logic
    public processInput = (input) => {
        const inputIsNumber: boolean = typeof input === 'number';

        // Capture the number of seconds between outputting the frequency of each number to the screen
        if (!this.interval) {
            if (inputIsNumber && input > 0) {
                this.setInterval(input);
                this.printLine('first');

                // repeat with the interval of N seconds
                this.timer = this.startTimer();
            } else {
                this.printLine('warning');
                this.printLine('greeting');
            }
        } else {
            // Handle user number input
            if (inputIsNumber) {
                if (this.isFib(input)) {
                    this.printLine('fib');
                }
                this.addToNumbersList(input);
                this.printLine('next');
            } else {
                // Handle text input
                switch (input) {
                    case 'quit':
                        this.printNumbersList();
                        this.printLine('quit');
                        // Program quit.
                        process.exit();
                        break;
                    case 'halt':
                        clearInterval(this.timer);
                        this.printLine('halt');
                        break;
                    case 'resume':
                        this.timer = this.startTimer();
                        this.printLine('resume');
                        break;
                    default:
                        this.printLine('warning');
                        this.printLine('next');
                        break;
                }
            }
        }
    };
}